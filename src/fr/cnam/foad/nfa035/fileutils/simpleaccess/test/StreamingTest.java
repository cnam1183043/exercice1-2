package fr.cnam.foad.nfa035.fileutils.simpleaccess.test;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming.ImageStreamingSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Classe de Test unitaire faite maison
 */
public class StreamingTest {

    /**
     * Test unitaire fait maison
     *
     * @param args
     */
     public static void main(String[] args) {
    	    try {
    	        File image = new File("petite_image.png");
    	        ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

    	        // Sérialisation                          
    	        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
    	        serializer.serialize(image, media);

    	        String encodedImage = media.getEncodedImageOutput().toString();
    	        System.out.println(encodedImage + "\n");

    	        // Désérialisation
    	        ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
    	        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

    	        deserializer.deserialize(media);
    	        byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
    	        // Vérification
    	        // 1/ Automatique
    	        byte[] originImage = Files.readAllBytes(image.toPath());
    	        assert Arrays.equals(originImage, deserializedImage);
    	        System.out.println("Cette sérialisation est bien réversible :)");

    	        //  2/ Manuelle
    	        File extractedImage = new File("petite_image_extraite.png");
    	        new FileOutputStream(extractedImage).write(deserializedImage);
    	        System.out.println("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");

    	    } catch (IOException e) {
    	        e.printStackTrace();
    	    }
    }

    /**
     * Méthode utile pour afficher une image sérialisée
     *
     * @param str
     * @param chars
     * @return
     */
    private static String splitDisplay(String str, int chars){
        StringBuffer strBuf = new StringBuffer();
        int i = 0;
        strBuf.append("================== Affichage de l'image encodée en Base64 ==================\n");
        for (; i+chars < str.length(); ){
            strBuf.append(str.substring(i,i+= chars));
            strBuf.append("\n");
        }
        strBuf.append(str.substring(i));
        strBuf.append("\n================================== FIN =====================================\n");

        return strBuf.toString();
    }
}
