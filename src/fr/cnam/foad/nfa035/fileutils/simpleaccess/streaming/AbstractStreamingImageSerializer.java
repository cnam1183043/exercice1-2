package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> 
{
	@Override 
	public final void serialize(S source, M media) throws IOException { 
		getSourceInputStream(source).transferTo(getSerializingStream(media)); 
	} 
}